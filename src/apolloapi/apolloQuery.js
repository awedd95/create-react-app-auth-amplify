import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

export const getTodo = gql`
  query getTodo($id: ID!) {
      getTodo(id: $id) {
        id
        name
      }
    }
`

export default graphql(getTodo)