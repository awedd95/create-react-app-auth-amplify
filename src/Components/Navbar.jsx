import React, {  useState, } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from '../views/Landing/landing';
import Login from '../views/Login/Login';
import Register from '../views/Register/Register';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Drawer from '@material-ui/core/Drawer';
import Dashboard from '../views/Dashboard/Dashboard';
import Jobs from '../views/Dashboard/Tasks';



function Nav(props){
	const[toggleLogin, setToggleLogin] = useState(false);
	const[toggleRegister, setToggleRegister] = useState(false);
 
  // props loggedIn and userData

  return (
    props.loggedIn?(
      <Router>
        <Switch>
            <Route exact path='/' component = {Dashboard} />
            <Route path='/jobs' component = {Jobs} />

           
        </Switch>
    </Router>
    
  ):(<Router>
  <AppBar position='fixed'>
  <Toolbar>
         <MenuItem><Link to={'/'}>Home</Link></MenuItem>
     <MenuItem onClick={() => setToggleLogin(true) }>Login</MenuItem>
     <MenuItem onClick={() => setToggleRegister(true) }>Register</MenuItem>
    </Toolbar>
    </AppBar>
    <Drawer anchor="right" open={toggleLogin} onClose={()=>setToggleLogin(false)} variant='temporary'><Login/></Drawer>
    <Drawer anchor="right" open={toggleRegister} onClose={()=>setToggleRegister(false)} variant='temporary'><Register/></Drawer>

  
        <hr />
        <Switch>
            <Route exact path='/' component = {Home} />
            
        </Switch>
    </Router>)
    );}

export default Nav;
