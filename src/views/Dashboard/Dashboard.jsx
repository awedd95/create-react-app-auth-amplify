import React, { Component, Fragment } from 'react';
import Amplify, { Auth } from 'aws-amplify';
import MiniDrawer from '../../Components/DashSideNav'

class Dashboard extends Component {
	
	render() {
		
	  return (
		  <Fragment>
		<div className="App">
		  <header className="App-header">
			<MiniDrawer/>
		  </header>
		</div>
		</Fragment>
	  );
	}
  }
  
  export default Dashboard;