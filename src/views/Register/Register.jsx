import React, { Component, useState } from 'react';
import { withAuthenticator } from 'aws-amplify-react'
import Amplify, { Auth } from 'aws-amplify';
import { Authenticator, SignUp } from 'aws-amplify-react/dist/Auth';
// import aws_exports from './aws-exports';
// Amplify.configure(aws_exports);
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import {Visibility, VisibilityOff, AccountCircleOutlined, EmailOutlined, ContactPhoneOutlined} from '@material-ui/icons';
import InputAdornment from '@material-ui/core/InputAdornment';

const formStyle= makeStyles({
	form:{
    maxWidth: 300,
    padding:20
	},
	field:{
    alignSelf:"center",
    padding: 10
	}


})

function Register() {
  const classes= formStyle();
	const [values, setValues] = useState({
		userName: '',
    password: '',
    phoneNumber:'',
    email:'',
		showPassword: false,

	});
		function handleSignUp(event){
			event.preventDefault();
			Auth.signUp({
        username:values.userName,
        password:values.password,
        attributes: {
          email:values.email,             // optional
          phone_number:values.phoneNumber,      // optional - E.164 number convention
          // Other custom attributes...
        },
        validationData: [],  // optional
        })
        .then(data => console.log(data))
        .catch(err => console.log(err));
		}
		const handleChange = event => {
			setValues({ ...values, [event.target.name]: event.target.value });
		};

		const handleClickShowPassword = () => {
			setValues({ ...values, showPassword: !values.showPassword });
		  };
 
    return (
      <form onSubmit={handleSignUp} className={classes.form}>			
		<TextField className={classes.field}
		variant="outlined"
		name="userName"
        type="text"
        label="Username"
        value={values.userName}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
               <AccountCircleOutlined/>
            </InputAdornment>
          ),
        }}
      />
      
			<TextField className={classes.field}
		variant="outlined"
		name="password"
        type={values.showPassword ? 'text' : 'password'}
        label="Password"
        value={values.password}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                edge="end"
                aria-label="Toggle password visibility"
                onClick={handleClickShowPassword}
              >
                {values.showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <TextField className={classes.field}
		variant="outlined"
		name="email"
        type="email"
        label="Email Address"
        value={values.email}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
               <EmailOutlined/>
            </InputAdornment>
          ),
        }}
      />
      <TextField className={classes.field}
		variant="outlined"
		name="phoneNumber"
        type="tel"
        label="Phone Number"
        value={values.phoneNumber}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
               <ContactPhoneOutlined/>
            </InputAdornment>
          ),
        }}
      />
			<Input type="submit" value="Submit" fullWidth="true"/>
		</form>
    );
  }


export default Register;
