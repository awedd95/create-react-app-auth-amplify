import React, { useEffect, useState, Fragment } from 'react';
import Amplify, { Auth } from 'aws-amplify';
import client from '../../apolloapi/apolloClient';
import gql from 'graphql-tag';
import Button from '@material-ui/core/Button/Button';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Typography, CircularProgress, Input, IconButton } from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import { createTodo } from '../../graphql/mutations';
import { onCreateTodo } from '../../graphql/subscriptions';

const StyledUp = makeStyles({
	ul: {
		listStyle: 'none',
		alignItems: "center",
		margin: 10,

	},
	root: {
		padding: (3, 2),
		alignItems: "center",
		textAlign: "center"
	},
})

export default function Jobs(props) {
	const classes = StyledUp()
	const [isLoading, setisLoading] = useState(false)
	const [Jobs, setJobs] = useState()
	const [refresh, setRefresh] = useState(false)
	const [values, setValues] = useState({
		name: '',
		description: ''
	})
	let result;
	if (Jobs) {
		result = Jobs.data.listTodos.items.map((item) =>
			<li key={item.id} className={classes.ul}><Typography>{item.name}</Typography> <Typography align='center' variant='5'>{item.description}</Typography></li>
		)
	}
	else {
		result = <li>No Jobs yet, try adding one to get started!</li>
	}

	const addTask = async (event) => {
		event.preventDefault();
		console.log("A form has been submitted")
		try {
			const result = await client.mutate({
				mutation: gql(createTodo),
				variables: {
					input: {
						name: values.name,
						description: values.description,
					}
				}
			}).then(
				((async () => {
					const newsub = await client.subscribe({ query: gql(onCreateTodo) }).subscribe({
						next: data => {
							console.log(data.data.onCreateTodo);
							setTimeout(() => {
								newsub.unsubscribe();
							}, 10000);
						},
						error: error => {
							console.warn(error);
						}
					});
				})))
			console.log(result.data.createTodo);

		}
		catch (error) {
			console.log(error)
		}
	}
	const container = () => {
		return (
			<Fragment >
				<form onSubmit={addTask}>
					<Input placeholder="Job Subject" onChange={(event) => setValues({ ...values, name: event.target.value })} fullWidth />
					<Input placeholder="Job Description" endAdornment={<IconButton
						color="inherit"
						aria-label="Open drawer"
						type="submit"
					>
						<Add />
					</IconButton>} onChange={(event) => setValues({ ...values, description: event.target.value })} fullWidth />
				</form>
				<ul className={classes.ul}>{result}</ul>
				<Button onClick={() => setRefresh(!refresh)}>Refresh</Button>
			</Fragment>
		)
	}
	useEffect(
		() => {
			const getJobList = async () => {
				setisLoading(true);
				try {
					setJobs(await client.query({
						query: gql`{
			  			listTodos(limit:5) {
							items {
					  		id
					  		name
					  		description
							}
						}
			  		}	
					`})
					)
				}
				catch (error) {
					console.log(error)
					//   // .then((value)=>{console.log(value)},(reason)=>{console.log(reason)})
				} setisLoading(false)
			}
			getJobList()
		}
		, [refresh])

	if (props.value === "mini") {
		return (<Paper className={classes.root}>
			{isLoading ? (<CircularProgress />) : (container())}</Paper>)
	} else {
	} {
		return (<Paper className={classes.root}>
			{isLoading ? (<CircularProgress />) : (<div className={classes.root}>
				<ul className={classes.ul}>{result}</ul><Button onClick={() => setRefresh(!refresh)}>Refresh</Button></div>)}</Paper>)

	}

}