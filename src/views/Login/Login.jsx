import React, { Component, useState } from 'react';
import { withAuthenticator } from 'aws-amplify-react'
import Amplify, { Auth } from 'aws-amplify';
// import { Authenticator, SignIn } from 'aws-amplify-react/dist/Auth';
// import aws_exports from './aws-exports';
// Amplify.configure(aws_exports);
import Input from '@material-ui/core/Input';
import { makeStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import {Visibility, VisibilityOff, AccountCircleOutlined} from '@material-ui/icons';
import InputAdornment from '@material-ui/core/InputAdornment';

const formStyle= makeStyles({
	form:{
		maxWidth: 300,
		padding:20
		},
		field:{
		alignSelf:"center",
		padding: 10
		}
})

function Login(){
	const classes= formStyle();
	const [values, setValues] = useState({
		userName: '',
		password: '',
		showPassword: false,

	});
		function handleLogin(event){
			event.preventDefault();
			Auth.signIn(values.userName, values.password)
				.then(user => console.log(user))
				.catch(err => console.log(err));
		}
		const handleChange = event => {
			setValues({ ...values, [event.target.name]: event.target.value });
		};

		const handleClickShowPassword = () => {
			setValues({ ...values, showPassword: !values.showPassword });
		  };

    return (
        <form onSubmit={handleLogin} className={classes.form}>			
		<TextField className={classes.field}
		variant="filled"
		name="userName"
        type="text"
        label="Username"
        value={values.userName}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
               <AccountCircleOutlined/>
            </InputAdornment>
          ),
        }}
      />
			<TextField className={classes.field}
		variant="filled"
		name="password"
        type={values.showPassword ? 'text' : 'password'}
        label="Password"
        value={values.password}
		onChange={handleChange}
		fullWidth="true"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                edge="end"
                aria-label="Toggle password visibility"
                onClick={handleClickShowPassword}
              >
                {values.showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
			<Input type="submit" value="Submit" fullWidth="true"/>
		</form>
     
    );
  }
export default Login;
