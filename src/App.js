import React, { Component, Fragment, useEffect, useState} from 'react';
import './App.css';
import { withAuthenticator } from 'aws-amplify-react'
import Amplify, { Auth, Hub } from 'aws-amplify';
import aws_exports from './aws-exports';
import Nav from './Components/Navbar'


Amplify.configure(aws_exports);

function signOut() {
Auth.signOut()
.then(data => console.log(data))
.catch(err => console.log(err));
}

function App(){
  const[isLoggedIn, setLoggedIn] = useState(false);
  const[userData, setUserData] = useState([]);

  async function checkUser() {
    await Auth.currentAuthenticatedUser()
    .then(user =>{
      setLoggedIn(true);
      setUserData(user)
    })
    .catch(err => {
      setLoggedIn(false);
       setUserData([])
    
    });
    }
  
    useEffect(() => {
      checkUser()
      Hub.listen('auth', (data) => {
        const { payload } = data
        console.log('A new auth event has happened: ', data)
         if (payload.event === 'signIn') {
           console.log('a user has signed in!')
           setLoggedIn(true);
           setUserData(payload)
         }
         if (payload.event === 'signOut') {
           console.log('a user has signed out!')
           setLoggedIn(false);
           setUserData([])
         }
      })
    }, [])

    return (
      <Fragment>
        <Nav loggedIn ={isLoggedIn} user={userData}/>
      </Fragment>
    );
  }


export default App;
